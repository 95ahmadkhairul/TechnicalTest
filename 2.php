<!DOCTYPE html>
	<html>
	<head>
	<meta charset="UTF-8">
		<title>Test No 2</title>
	</head>
	<style>
	body{
		font-family: sans-serif;
		font-size: 18px;
		color: white;
		background-color: #b3ecff;
	}
	/* source code for box https://codepen.io/FrankieDoodie/pen/GXWwVv */
	.box{
		position: absolute;
		top:50%;
		left: 50%;
		transform: translate(-50%, -50%);
		width: 40rem;
		padding: 2.5rem;
		box-sizing: border-box;
		background: rgba(0, 0, 0, 0.6);
		border-radius: 0.625rem;
	}
	p{
		line-height: 2;
	}
	input{
  		font-family: sans-serif;
		font-size: 18px;
		width: 200px;
	}
	.submit{
		background-color: #4da6ff;
		color: white;
		padding: 5px 15px;
		text-align: center;
		font-size: 16px;
		border-radius: 12px;
	}
	</style>
	<body>
		<div class="box">
		<form method="post" action="2.php">
			<p>Username<br/>
			<input type="text" name="nama"> </p>

			<p>Email<br/>
			<input type="text" name="email"> </p>
			
			<p>Password<br/>
			<input type="password" name="pass"> </p>
			<p> <input type="submit" class="submit" name="submit" value="Submit"> </p>
		</form>
		<p>Hasil : <br />
			
		<?php
			if(isset($_POST['submit'])){
				$nama = $_POST['nama'];
				$email = $_POST['email'];
				$pass = $_POST['pass'];

				if(preg_match("/[a-z]{6}/", $nama)){
					$nama .= " Nama valid <br />";
				}else{
					$nama .= " Nama tidak valid <br />";
				}

				if(preg_match("/[a-z](?=.*@)/", $email)){
					$email .= " Email valid <br />";
				}else{
					$email .= " Email tidak valid <br />";
				}


				if(preg_match("/(?=[^A-Z]*[A-Z])(?=[^0-9]*[0-9]){8,}/", $pass)){
					$pass .= " Password valid <br />";
				}else{
					$pass .= " Password tidak valid <br />";
				}

				echo $nama.$email.$pass;
			}
		?>
		</p>
	</div>
	</body>
</html>
				
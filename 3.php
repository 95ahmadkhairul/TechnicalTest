<!DOCTYPE html>
	<html>
	<head>
	<meta charset="UTF-8">
		<title>Test No 3</title>
	</head>
	<style>
	body{
		font-family: sans-serif;
		font-size: 18px;
		color: white;
		background-color: #b3ecff;
	}
	/* source code for box https://codepen.io/FrankieDoodie/pen/GXWwVv */
	.box{
		position: absolute;
		top:50%;
		left: 50%;
		transform: translate(-50%, -50%);
		width: 40rem;
		padding: 2.5rem;
		box-sizing: border-box;
		background: rgba(0, 0, 0, 0.6);
		border-radius: 0.625rem;
	}
	p{
		line-height: 2;
	}
	input{
  		font-family: sans-serif;
		font-size: 18px;
		width: 200px;
	}
	.submit{
		background-color: #4da6ff;
		color: white;
		padding: 5px 15px;
		text-align: center;
		font-size: 16px;
		border-radius: 12px;
	}
	</style>
	<body>
		<div class="box">
		<form method="post" action="3.php">
			<p>Kolom<br/>
			<input type="number" name="kolom"> </p>

			<p>Baris<br/>
			<input type="number" name="baris"> </p>

			<p> <input type="submit" class="submit" name="submit" value="Submit"> </p>
		</form>
		<p>Hasil : <br />
			
		<?php
			if(isset($_POST['submit'])){
				$kolom = $_POST['kolom'];
				$baris = $_POST['baris'];

				//echo $kolom.$baris;
				$stop = $kolom*$baris+1;
				$n = 10000; 
				$total = 1;
				for ($i=1;$i<=$n;$i++){
					$a=0;
					for($j=1;$j<=$i;$j++){
						if($i % $j == 0){
							$a++;
						}
					}
					if($a == 2 && $total != $stop){
						if ($total%$kolom==0){
							$total++;
							echo $i."<br />";
						}else{
							$total++;
							echo $i." ";
						}
					}
				}
			}
		?>
		</p>
	</div>
	</body>
</html>
				
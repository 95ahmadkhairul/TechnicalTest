/*
 Navicat Premium Data Transfer

 Source Server         : TEST
 Source Server Type    : MySQL
 Source Server Version : 100408
 Source Host           : localhost:3306
 Source Schema         : count

 Target Server Type    : MySQL
 Target Server Version : 100408
 File Encoding         : 65001

 Date: 21/12/2019 21:49:34
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for customers
-- ----------------------------
DROP TABLE IF EXISTS `customers`;
CREATE TABLE `customers`  (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `last_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `state` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `zip_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of customers
-- ----------------------------
INSERT INTO `customers` VALUES (1, 'ahmad', 'khairul', 'khairul@gmail.com', 'bandung', 'bandung', 'indonesia', '40044');
INSERT INTO `customers` VALUES (2, 'faishal', 'faiq', 'faishal@gmail.com', 'bandung', 'bandung', 'indonesia', '40044');

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders`  (
  `id` int(11) NOT NULL,
  `customer_id` int(10) NULL DEFAULT NULL,
  `order_placed_date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `tb_customer_ibfk_1`(`customer_id`) USING BTREE,
  CONSTRAINT `tb_customer_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES (1, 1, '2018-11-28 21:04:39');
INSERT INTO `orders` VALUES (2, 2, '2019-12-17 21:04:36');
INSERT INTO `orders` VALUES (3, 1, '2019-12-09 21:04:33');
INSERT INTO `orders` VALUES (4, 2, '2019-12-10 21:04:29');
INSERT INTO `orders` VALUES (5, 2, '2019-12-20 21:04:22');
INSERT INTO `orders` VALUES (6, 1, '2019-12-26 21:04:25');

-- ----------------------------
-- Table structure for tb_caleg
-- ----------------------------
DROP TABLE IF EXISTS `tb_caleg`;
CREATE TABLE `tb_caleg`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `id_partai` int(11) NULL DEFAULT NULL,
  `earned_vote` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id_partai`(`id_partai`) USING BTREE,
  CONSTRAINT `tb_caleg_ibfk_1` FOREIGN KEY (`id_partai`) REFERENCES `tb_partai` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_caleg
-- ----------------------------
INSERT INTO `tb_caleg` VALUES (1, 'Is Bos', 2, 74);
INSERT INTO `tb_caleg` VALUES (2, 'Dobleh', 1, 104);
INSERT INTO `tb_caleg` VALUES (3, 'Kabur', 3, 73);

-- ----------------------------
-- Table structure for tb_partai
-- ----------------------------
DROP TABLE IF EXISTS `tb_partai`;
CREATE TABLE `tb_partai`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_partai
-- ----------------------------
INSERT INTO `tb_partai` VALUES (1, 'Cilacap Santai');
INSERT INTO `tb_partai` VALUES (2, 'Cilacap Makmur');
INSERT INTO `tb_partai` VALUES (3, 'Cilacap Aman');

SET FOREIGN_KEY_CHECKS = 1;

<!DOCTYPE html>
	<html>
	<head>
	<meta charset="UTF-8">
		<title>Test No 4</title>
	</head>
	<style>
	body{
		font-family: sans-serif;
		font-size: 18px;
		color: white;
		background-color: #b3ecff;
	}
	/* source code for box https://codepen.io/FrankieDoodie/pen/GXWwVv */
	.box{
		position: absolute;
		top:50%;
		left: 50%;
		transform: translate(-50%, -50%);
		width: 40rem;
		padding: 2.5rem;
		box-sizing: border-box;
		background: rgba(0, 0, 0, 0.6);
		border-radius: 0.625rem;
	}
	p{
		line-height: 2;
	}
	input{
  		font-family: sans-serif;
		font-size: 18px;
		width: 200px;
	}
	.submit{
		background-color: #4da6ff;
		color: white;
		padding: 5px 15px;
		text-align: center;
		font-size: 16px;
		border-radius: 12px;
	}
	</style>
	<body>
		<div class="box">	
		<?php
			function Sortir($array){
				$n = count($array);
				sort($array);

				for($i=0; $i<$n; $i++){
	            	sort($array[$i]);
	            	$arr[$i] = $array[$i];
	    		}
	    		echo "<p>";
	    		print_r($arr);
	    		echo "</p>";
    		}

    		$data = array(['d','c','e','b','a'],['k','i','j']);
    		Sortir($data);

    		$datalain = array(['g','h','i','j'],['a','c','b','e','d'],['g','e','f']);
    		Sortir($datalain);
		?>
		</div>
  	</body>
</html>
	
Project Technical Test ini merupakan jawaban dari Test Technical Bootcamp DumbWays.id pada tanggal 21 - 12 - 2019.
Bahasa yang digunakan adalah PHP

Cara menjalankan program
1. buat database bernama count
2. dump count.sql untuk otomatis membuat tabel yang terkait dengan program
3. jalankan program dengan mengaksesnya via localhost 127.0.0.1/[nama file]

Daftar Program
1. 1.html : Tampilan web page HTML + CSS sesuai mockup yang diberikan
1. 2.php : Web login page dibuat menggunakan bahasa pemograman PHP dengan kriteria validasi yang ditetapkan
1. 3.php : Program pencetak deret bilangan prima dengan input baris dan kolom output berupa deretan bilangan prima
1. 4.php : Function untuk mengurutkan array
1. 5.txt : bukan program, melainkan kumpulan query sql yang merupakan dari jawaban dari soal berupa kumpulan perintah yang diharuskan untuk diubah kedalam bentuk query sql
1. 6.php : Program voting online untuk vote calon legislatif favorite kamu
1. 7.txt : bukan program juga, melainkan 1 buah sintax yang 'rumit' dari sebuah studi kasus

